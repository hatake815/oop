package week2.Kingdom_With_Weapons;

public class Main {
    public static void main(String[] args) {
        Character character = new Knight();
        character.Fight();
        character.SetWeapon(new BowAndArrowBehavior());
        character.Fight();

        character = new King();
        character.Fight();
        character.SetWeapon(new AxeBehavior());
        character.Fight();
    }
}
