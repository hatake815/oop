package week2.Kingdom_With_Weapons;

public interface WeaponBehavior
{
    void UseWeapon();
}
