package week2.Kingdom_With_Weapons;

public abstract class Character {
    WeaponBehavior weapon;

    public void Fight() {
        weapon.UseWeapon();
    }
    public void SetWeapon(WeaponBehavior weapon) {
        this.weapon = weapon;
    }
}

