package week2.Observer;

public class Main {
    public static void main(String[] args) {
        JobMagazine hello = new JobMagazine();
        hello.addVacancy("teacher");
        hello.addVacancy("singer");
        hello.addVacancy("copywriter");

        Subscriber alisher = new Subscriber("Alisher");
        Subscriber venera = new Subscriber("venera");

        hello.addObserver(alisher);
        hello.addObserver(venera);

        hello.notifyAllObservers();
    }
}
