package term2.Factory;

public class Main {
    public static void main(String[] args) {
        Factory factory = new Factory();
        Guitar fender = factory.create("Fender");
        Guitar gibson = factory.create("Gibson");
        Guitar yamaha = factory.create("Yamaha");
        fender.tune();
        gibson.tune();
        yamaha.tune();
        fender.play();
        gibson.play();
        yamaha.play();
    }

    interface Guitar{
        void play();
        void tune();
    }

    static class Fender implements Guitar{

        @Override
        public void play() {
            System.out.println("play Fender");
        }

        @Override
        public void tune() {
            System.out.println("Tuning Fender");
        }
    }

    static class Gibson implements Guitar{

        @Override
        public void play() {
            System.out.println("play Gibson");
        }

        @Override
        public void tune() {
            System.out.println("Tuning Gibson");
        }
    }

    static class Yamaha implements Guitar{

        @Override
        public void play() {
            System.out.println("play Yamaha");
        }

        @Override
        public void tune() {
            System.out.println("Tuning Yamaha");
        }
    }

    static class Factory {
        public Guitar create(String typeOfGuitar){
            switch (typeOfGuitar){
                case "Fender" : return new Fender();
                case "Gibson" : return new Gibson();
                case "Yamaha" : return new Yamaha();
                default: return null;
            }
        }
    }

}
