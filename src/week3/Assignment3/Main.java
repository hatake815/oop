package week3.Assignment3;

public class Main {
    public static void main(String[] args) {

        Guitar fender = new Guitar("Fender");

        fender.ConnectGuitar();
        fender.TurnOnAmp();

        try {
            fender.play();
        } catch (YouAreNotReady e) {
            e.printStackTrace();
        }
    }

}
