package week3.Assignment3;

public class Guitar {
    private String name;
    private boolean isGuitarConnected;
    private boolean isAmpTurnedOn;
    private boolean isPickUpInHand;
    private String problem;

    public Guitar(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void ConnectGuitar() {
        isGuitarConnected = true;
        System.out.println("Your guitar connected!");
    }

    public void TurnOnAmp() {
        isAmpTurnedOn = true;
        System.out.println("Your amplifier turned on!");
    }

    public void GetPickUp() {
        isPickUpInHand = true;
        System.out.println("You are ready to rock!");
    }

    public void play() throws YouAreNotReady {
        if (isPickUpInHand && isAmpTurnedOn && isGuitarConnected) {
            System.out.println("Play me Victor Coi songs with your " + name + "!");
        } else {
            throw new YouAreNotReady("You cannot play on your "+name);
        }
    }
    //wrong commit
    //It should be assignment 4
}
