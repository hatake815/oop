package week3.Assignment3;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;


public class YouAreNotReady extends Exception {

    public YouAreNotReady(String message) {

        super(message);

        PrintWriter out = null;
        try {
            FileOutputStream myFile = new FileOutputStream("Exception.txt");
            out = new PrintWriter(myFile);
            out.print(message);
            out.close();
        } catch (IOException e) {
            System.out.println("Nothing to write!");
        } finally {
            out.close();
        }
    }

}
