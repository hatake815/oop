package week3.Whether_Station;

public class StatisticsDisplay implements Observer, DisplayElement {

    private double maxTemp = 0f;
    private double minTemp = 200;
    private double sumTemp = 0f;
    private int num = 0;
    private Subject weatherData;

    public StatisticsDisplay(Subject weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    @Override
    public void display() {
        System.out.println("Avg temperature = " + sumTemp/num);
        System.out.println("Max temperature = " + maxTemp);
        System.out.println("Min temperature = " +minTemp);
        System.out.println("_______________________________________________________");
        System.out.println();

    }

    @Override
    public void update(double temperature, double humidity, double pressure) {
        if (maxTemp < temperature) {
            maxTemp = temperature;
        }
        if (minTemp > temperature) {
            minTemp = temperature;
        }
        sumTemp += temperature;
        num++;
        display();
    }
}
