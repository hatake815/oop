package week3.Whether_Station;

public class ForecastDisplay implements Observer, DisplayElement {

    private double lastPressure;
    private double currPressure = 29;
    private Subject weatherData;

    public ForecastDisplay(Subject weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    @Override
    public void display() {
        System.out.print("Forecast: ");
        if (currPressure > lastPressure) {
            System.out.println("Improving weather on the way!");
        } else if (currPressure < lastPressure) {
            System.out.println("Watch out for cooler, rainy weather");
        } else System.out.println("More of the same");
    }

    @Override
    public void update(double temperature, double humidity, double pressure) {
        lastPressure = currPressure;
        currPressure = pressure;
        display();
    }
}
