package week3.Whether_Station;

public interface Observer {
    void update(double temp, double humidity, double pressure);
}
