package week3.Controllers;

public interface Controller {
    void connect();
    void keyPressed();
    //интерфейс создает контракт
    //interface doesn't have a body
    //all methods abstract
}
