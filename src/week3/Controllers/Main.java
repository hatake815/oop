package week3.Controllers;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Controller controller1 = new Keyboard();
        Controller controller2 = new GameController();

        List<String> controllers = new ArrayList<>();
        controller1.connect();
    }
}
