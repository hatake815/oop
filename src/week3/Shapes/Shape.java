package week3.Shapes;

public abstract class Shape {
    int edges = 0;

    abstract void display();
    abstract void area();
}
