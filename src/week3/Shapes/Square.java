package week3.Shapes;
//final отменяет extend и создает константу если это поле
//final static int
public class Square extends Shape {
    int side = 5;

    @Override
    void display() {
        System.out.println("Drawing square..");
    }

    @Override
    void area() {
        System.out.println("Area of square:"+side*side);
    }
}
