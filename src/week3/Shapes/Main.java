package week3.Shapes;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle();
        Square square = new Square();
        List<Shape> createdShapes = new ArrayList<>();
        createdShapes.add(circle);
        createdShapes.add(square);

        for (Shape shape: createdShapes){
            shape.display();
        }

    }
}
