package week1.ducks;

public class FlyNoWay implements Flyable{
    @Override
    public void fly() {
        System.out.println("I'm not Flying!");
    }
}
