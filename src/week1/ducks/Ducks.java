package week1.ducks;

public abstract class Ducks {

    Quackable quackBehavior;
    Flyable flyBehavior;

    public void performQuack(){
        quackBehavior.quack();
    }

    public void performFly(){
        flyBehavior.fly();
    }

    public void swim(){}
    public void display(){
        System.out.println("I'm a Duck!");
    }
}
