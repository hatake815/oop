package week1.ducks;

public interface Quackable {
    void quack();
}
