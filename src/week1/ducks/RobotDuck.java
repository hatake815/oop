package week1.ducks;

public class RobotDuck extends Ducks {
    public RobotDuck() {
        flyBehavior = new FlyWithEngine();
        quackBehavior = new Quack();
    }

    @Override
    public void display() {
        System.out.println("I'm a Robot Duck!");
    }
}
