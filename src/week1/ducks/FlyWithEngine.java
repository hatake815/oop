package week1.ducks;

public class FlyWithEngine extends Ducks implements Flyable{
    @Override
    public void fly() {
        System.out.println("I'm Flying with Engine!");
    }
}

