package week1.ducks;

public class RubberDuck extends Ducks {

    public RubberDuck(){
        quackBehavior = new Squeck();
        flyBehavior = new FlyNoWay();
    }

    @Override
    public void display() {
        System.out.println("I'm a Rubber Duck");
    }

}
