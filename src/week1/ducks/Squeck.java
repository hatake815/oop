package week1.ducks;

public class Squeck implements Quackable {
    @Override
    public void quack() {
        System.out.println("I'm Squeaking!");
    }
}
