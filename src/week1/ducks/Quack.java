package week1.ducks;

public class Quack implements Quackable {
    @Override
    public void quack() {
        System.out.println("I'm Quacking!");
    }
}
