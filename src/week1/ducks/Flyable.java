package week1.ducks;

public interface Flyable {
    void fly();
}
