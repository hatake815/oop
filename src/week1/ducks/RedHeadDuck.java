package week1.ducks;

public class RedHeadDuck extends Ducks{

    public RedHeadDuck(){
        quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();
    }

    @Override
    public void display() {
        System.out.println("I'm a Red Head Duck!");
    }

}
