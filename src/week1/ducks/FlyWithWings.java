package week1.ducks;

public class FlyWithWings implements Flyable {
    @Override
    public void fly() {
        System.out.println("I'm Flying with Wings!");
    }
}
