package week1.homework;

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person();
        person1.input("Alisher",2000);

        Person person2 = new Person();
        person2.input("Azamat",2002);

        Person person3 = new Person();
        person3.input("Erbolat",2001);

        Person person4 = new Person();
        person4.input("Diana",2003);

        Person person5 = new Person();
        person5.input("Tomiris",2000);

        person1.age();
        person2.age();
        person3.age();
        person4.age();
        person5.age();
        System.out.println();

        person1.output();
        person2.output();
        person3.output();
        person4.output();
        person5.output();
        System.out.println();

        person1.changeName("Nurbot1");
        person2.changeName("Nurbot2");
        person3.changeName("Nurbot3");
        person4.changeName("Nurbot4");
        person5.changeName("Nurbot5");

        person1.output();
        person2.output();
        person3.output();
        person4.output();
        person5.output();
    }
}
