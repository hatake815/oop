package week1.homework;

public class Person {
    private String name;
    private int BirthYear;
    private int currentYear = 2020;

    public Person(){
    }

    public Person(String name, int birthYear) {
        this.name = name;
        BirthYear = birthYear;
    }

    public void age(){
        System.out.println(currentYear-BirthYear);
    }

    public void input(String name, int birthYear){
        this.name=name;
        this.BirthYear=birthYear;
    }

    public void output(){
        System.out.println(name + " " + BirthYear);
    }

    public void changeName(String name){
        this.name=name;
    }
}