package week1.example;

public class Student {
    private String name;
    private double rating;
    static double avgRating;
    static double totalRating = 0.0;
    static int count = 0;

    public Student(){
        count++;
    }

    public Student(String name, double rating){
        this.rating = rating;
        totalRating += rating;
        this.name = name;
        count++;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
        totalRating += rating;
    }

    static double getAvgRating(){
        if (count==0){
            return 0;
        }
        avgRating = totalRating/count;
        return avgRating;
    }

    static double getTotalRating(){
        return totalRating;
    }

    static String betterStudent(Student student1, Student student2) {
        if (student1.getRating()>student2.getRating()){
            return student1.getName();
        } else if (student1.getRating()<student2.getRating()){
            return student2.getName();
        }
        return "They both have same Ratings";
    }

    @Override
    public String toString(){
        return "Student [name= "+getName()+", rating= " +getRating()+"]";
    }

    public void changeRating(double rating){
        if (this.rating!=0.0) {
            totalRating-= this.rating;
            totalRating+=rating;
            this.rating = rating;
        } else {
            totalRating+=rating;
            this.rating = rating;
        }
    }
}
