package week1.example;

public class Main {
    public static void main(String[] args) {
        Student student1 = new Student("Alisher",4.0);
        Student student2 = new Student("Bekzat",3.75);

        student1.changeRating(0.0);
        student2.changeRating(4.0);

        System.out.println(Student.getAvgRating());
        System.out.println(Student.getTotalRating());
    }
}
