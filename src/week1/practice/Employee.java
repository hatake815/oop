package week1.practice;

public class Employee {
    private String name;
    private double rate;
    private double hours;
    static double totalSum = 0;
    static double totalHour = 0;

    public Employee(){
        totalSum++;
    }

    public Employee(String name, double rate){
        this.name = name;
        this.rate = rate;
        totalSum++;
    }

    public Employee(String name, double rate, double hours){
        this.name = name;
        this.rate = rate;
        this.hours = hours;
        totalSum++;
        totalHour = totalHour+hours;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getHours() {
        return hours;
    }

    public double getRate() {
        return rate;
    }

    static double getTotalSum() {
        return totalSum;
    }

    public void setHours(double hours) {
        totalHour = totalHour-this.hours;
        totalHour = totalHour+hours;
        this.hours = hours;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double knowSalary(){
        double salary = this.rate * this.hours;
        return salary;
    }

    @Override
    public String toString(){
        return "Employee [ name: "+name+", rate: "+rate+", hours: "+ hours+"]";
    }

    public void changeRate(double rate){
        this.rate = rate;
    }

    public double bonuses(){
        double salary = knowSalary();
        return salary/10;
    }

    static double getTotalHour(){
        return totalHour;
    }
}
