package week1.practice;

public class Main {
    public static void main(String[] args) {
        Employee employee1 = new Employee("Alisher",14.5,30);
        Employee employee2 = new Employee("Azamat",11.6,35);
        Employee employee3 = new Employee("Zhandos",18.5,25);

        employee1.setHours(40);
        employee2.setHours(40);
        employee3.setHours(40);

        System.out.println(employee1.knowSalary());
        System.out.println(employee1);
        System.out.println(employee1.bonuses());
        System.out.println(Employee.getTotalSum());

        System.out.println(Employee.getTotalHour());
        System.out.println(Employee.getTotalSum());
    }
}
